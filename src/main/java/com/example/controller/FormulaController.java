package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.example.domain.ExecuteParam;
import com.example.formula.FormulaComponent;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import jakarta.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Map;

@Controller
public class FormulaController {

  protected static final Logger LOGGER = LoggerFactory.getLogger(FormulaController.class);

  @Inject
  protected FormulaComponent formulaComponent;

  /**
   * 计算单元格公式
   *
   * @param executeParam 单元格映射/公式
   * @return 计算结果或异常信息
   */
  @Post(value = "/execute")
  public Object execute(@Body ExecuteParam executeParam) {
    Map<String, String> cellMap = executeParam.getCellMap();
    String formula = executeParam.getFormula();
    try {
      // 执行目标脚本
      return returnSuccess(formulaComponent.executeFormula(cellMap, formula));
    } catch (Throwable e) {
      // 记录日志
      LOGGER.error("Error to execute the formula: {}, cellMap: {}", formula, JSON.toJSONString(cellMap), e);
      // 返回异常提示信息
      return returnError("Sorry, could not resolve the formula you inputted for now.");
    }
  }

  public Object returnError(String errorMsg) {
    Map<String, Object> result = new LinkedHashMap<>();
    result.put("success", false);
    result.put("errorMsg", errorMsg);
    return result;
  }

  public Object returnSuccess(Object backVal) {
    Map<String, Object> result = new LinkedHashMap<>();
    result.put("success", true);
    result.put("result", backVal);
    return result;
  }


}
