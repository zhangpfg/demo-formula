package com.example.controller;

import com.example.domain.User;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller(value = "/hello",consumes = MediaType.APPLICATION_JSON)
public class HelloController {
    @Get()
    public User test(){
        User user = new User();
        user.setName("张成");
        return user;
    }
}
