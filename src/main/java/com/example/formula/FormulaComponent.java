package com.example.formula;


import com.example.g4.FormulaLexer;
import com.example.g4.FormulaParser;
import io.micronaut.context.annotation.Bean;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.openjdk.nashorn.api.scripting.NashornScriptEngineFactory;

import javax.script.ScriptEngine;
import java.io.FileReader;
import java.io.Reader;
import java.util.Map;

@Bean
public class FormulaComponent {
    NashornScriptEngineFactory factory = new NashornScriptEngineFactory();

    public Object executeFormula(Map<String, String> cellMap, String formula) {
        String directive = null;
        try {
            directive = parseFormula(cellMap, formula);
            Reader reader = new FileReader("src/main/java/com/example/js/formula.min.js");

            ScriptEngine scriptEngine1 = factory.getScriptEngine();
            scriptEngine1
                    .eval(reader);
            return scriptEngine1.eval(directive);
        } catch (Throwable e) {
            throw new RuntimeException("Execute the script error: " + formula + ", directive: " + directive, e);
        }
    }

    public String parseFormula(Map<String, String> cellMap, String formula) {
        // 造词法分析器
        FormulaLexer formulaLexer = new FormulaLexer(CharStreams.fromString(formula));
        // 用词法分析器构造Token流
        TokenStream tokenStream = new CommonTokenStream(formulaLexer);
        // 用Token流构造语法解析器
        FormulaParser parser = new FormulaParser(tokenStream);
        // 添加异常监听器
        parser.addErrorListener(FormulaParserErrorListener.instance);
        // 生成抽象语法树
        FormulaParser.FormulaContext context = parser.formula();
        // 访问抽象语法树
        // 1.从中提取出表示单元格的ID并替换成目标单元格的值
        // 2.将函数替换成目标函数
        MFormulaParserVisitor parserVisitor = new MFormulaParserVisitor(cellMap);
        return parserVisitor.accept(context);
    }

}
