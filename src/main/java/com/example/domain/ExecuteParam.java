package com.example.domain;

import java.util.Map;


public class ExecuteParam {


    /**
     * 单元格映射
     */
    protected Map<String, String> cellMap;

    /**
     * 公式
     */
    protected String formula;

    public Map<String, String> getCellMap() {
        return cellMap;
    }

    public void setCellMap(Map<String, String> cellMap) {
        this.cellMap = cellMap;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }


}
