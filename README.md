## 主页有联系方式，欢迎来交流
### 用formula.js编写一个在JVM里运行的Excel公式计算引擎服务要求如下：
- 1 formula.js必须使用GraalVM运行
- 2 使用Micronaut框架构建服务
- 3 服务要有至少两个操作数参数A1和A2以及一个公式参数
- 4 公式参数可以输入形如A1+A2，IF(A1=0，A2， "0")这样的Excel公式
- 5 POST到服务后出计算结果
- 6 输入和输出用请用JSON格式

参考
- https://github.com/formulajs/formulajs
- https://www.graalvm.org/
- https://micronaut.io/
## Micronaut 3.8.4 Documentation

- [User Guide](https://docs.micronaut.io/3.8.4/guide/index.html)
- [API Reference](https://docs.micronaut.io/3.8.4/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/3.8.4/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)

---

- [Shadow Gradle Plugin](https://plugins.gradle.org/plugin/com.github.johnrengelman.shadow)

## Feature http-client documentation

- [Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)


